﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using CuentaTiempo.Validadores;
using LiteDB;

namespace CuentaTiempo.DAL
{
    public class RepositorioGenerico<T> where T:Base
    {
        private string DBName;
        private BaseValidator<T> validator;
        public string Error { get; protected set; }
        public RepositorioGenerico(string path, BaseValidator<T> validador)
        {
            validator = validador;
            DBName=Path.Combine(path, "CuentaTimepo.db");
        }
        
        public IEnumerable<T> Get
        {
            get
            {
                try
                {
                    List<T> datos = new List<T>();
                    //abrir conexion con la BD
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        datos = col.FindAll().ToList();
                    }
                    Error = "";
                    return datos;
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
                

            }
        }
        public T GetById(string id)
        {
            try
            {
                T dato;
                //abrir conexion con la BD
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    dato = col.FindById(id);
                }
                Error = "";
                return dato;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public T Create(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                entidad.FechaHora = DateTime.Now;
                var resultadoValidacion = validator.Validate(entidad);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Insert(entidad);
                    }
                    Error = "";
                    return entidad;
                }
                else
                {
                    Error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        Error += item.ErrorMessage + ". ";
                    }
                    return null;
                }
                
                
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public T Update(string id, T entidad)
        {
            try
            {
                entidad.FechaHora = DateTime.Now;
                var resultadoValidacion = validator.Validate(entidad);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Update(id, entidad);
                    }
                    Error = "";
                    return entidad;
                }
                else
                {
                    Error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        Error += item.ErrorMessage + ". ";
                    }
                    return null;
                }


            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                bool r;
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    r=col.Delete(id);
                }
                    Error = "";
                    return r;
                
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        
        public IEnumerable<T> Query(Expression<Func<T, bool>> query)
        {
            try
            {
                List<T> datos = new List<T>();
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    datos = col.Find(query).ToList();
                }
                Error = "";
                return datos;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}

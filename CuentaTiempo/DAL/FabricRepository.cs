﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using CuentaTiempo.Validadores;

namespace CuentaTiempo.DAL
{
    public static class FabricRepository
    {
        public static string Path;
        public static RepositorioGenerico<Cliente> RepositorioCliente()=>new RepositorioGenerico<Cliente>(Path, new ClienteValidator());
        public static RepositorioGenerico<Proyecto> RepositorioProyecto()=>new RepositorioGenerico<Proyecto>(Path, new ProyectoValidator());
        public static RepositorioGenerico<CostoPorHora> RepositorioCostoPorHora() => new RepositorioGenerico<CostoPorHora>(Path, new CostoPorHoraValidator());
        public static RepositorioGenerico<Extras> RepositorioExtras() => new RepositorioGenerico<Extras>(Path, new ExtrasValidator());
        public static RepositorioGenerico<TrabajoRealizado> RepositorioTrabajoRealizado() => new RepositorioGenerico<TrabajoRealizado>(Path, new TrabajoRealizadoValidator());
        public static RepositorioGenerico<OrdenDePago> RepositorioOrdenDePago() => new RepositorioGenerico<OrdenDePago>(Path, new OrdenDePagoValidator());
    }
}

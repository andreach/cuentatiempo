﻿using CuentaTiempo.DAL;
namespace CuentaTiempo;

public partial class App : Application
{
	public App()
	{
		InitializeComponent();
		FabricRepository.Path=FileSystem.Current.AppDataDirectory;
		MainPage = new AppShell();
	}
}

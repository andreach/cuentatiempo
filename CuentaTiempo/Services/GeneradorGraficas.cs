﻿using CuentaTiempo.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Services
{
    public static class GeneradorGraficas
    {
        public static string GeneraGrafica(List<DatoGrafica> datos, string titulo, string tipo, int ancho=-1, int alto = -1)
        {
            StringBuilder html = new StringBuilder();
            html.Append($"<html><head><script src='https://cdn.plot.ly/plotly-2.20.0.min.js'></script></head><body><div id='myDiv' style=\"");
            if (alto!= -1)
            {
                html.Append($"height: {alto}px; ");
            }
            html.Append("width: ");
            if (ancho!=-1)
            {
                html.Append($"width: {ancho}px; ");
            }
            html.Append("\"><div><script>");
            html.Append("var data=[{x:[");
            foreach (var item in datos)
            {
                html.Append($"'{item.X}',");
            }
            html.Append("],y:[");
            foreach (var item in datos)
            {
                html.Append($"'{item.Y}',");
            }
            html.Append("],type:'");
            html.Append(tipo);
            html.Append("'}]; ");
            html.Append("var layout={title:'");
            html.Append(titulo);
            html.Append("'}; ");
            html.Append("Plotly.newPlot('myDiv',data,layout);");
            html.Append("</script></body><html>");

            return html.ToString();
        }
        public static string GeneradorGraficaVariasSeries(List<SerieDeGrafica> datos, string titulo, int ancho=-1, int alto = -1)
        {
            StringBuilder html = new StringBuilder();
            html.Append($"<html><head><script src='https://cdn.plot.ly/plotly-2.20.0.min.js'></script></head><body><div id='myDiv' style=\"");
            if (alto != -1)
            {
                html.Append($"height: {alto}px; ");
            }
            html.Append("width: ");
            if (ancho != -1)
            {
                html.Append($"width: {ancho}px; ");
            }
            html.Append("\"><div><script>");
            int s = 0;
            foreach (var serie in datos)
            {
                html.Append($"var trace{s}=");
                html.Append("{x:[");
                foreach (var item in serie.Datos)
                {
                    html.Append($"{item.X},");
                }
                html.Append("],y:[");
                foreach (var item in serie.Datos)
                {
                    html.Append($"{item.Y},");
                }
                html.Append("], name: ");
                html.Append($"'{serie.Tipo}'");
                html.Append("}; ");
                s++;
            }
            html.Append("var layout={title: ");
            html.Append(titulo);
            html.Append("'}; ");
            html.Append("var data=[");
            for (int i = 0; i < datos.Count; i++)
            {
                html.Append($"trace{i},");
            }
            html.Append("]; ");
            html.Append("plotly.newPlot('myDiv',data,layout);");
            html.Append("</script></body></html>");

            return html.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Entidades
{
    public class Extras:Base
    {
        public string IdProyecto { get; set; }
        public DateTime FechaHoraEvento { get; set; }
        public string Descripcion { get; set; }
        public float Costo { get; set; }
        public string Notas { get; set; }
        public string IdOrdenDePago { get; set; }
    }
}

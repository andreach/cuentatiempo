﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Entidades
{
    public class Proyecto:Base
    {
        public string Nombre { get; set; }
        public string IdCliente { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaTermino { get; set; }
        public override string ToString()
        {
            return Nombre;
        }
        
    }
    
}

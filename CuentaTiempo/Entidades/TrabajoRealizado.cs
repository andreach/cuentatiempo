﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Entidades
{
    public class TrabajoRealizado:Base
    {
        public string IdProyecto { get; set; }
        public string IdCostoPorHora { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime? Termino { get; set; }
        public string Descripcion { get; set; }
        public string IdOrdenDePago { get; set; }
    
        public override string ToString()
        {
            return Descripcion;
        }
    }
}

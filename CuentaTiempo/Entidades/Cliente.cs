﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Entidades
{
    public class Cliente:Base
    {
        public string Nombre { get; set; }
        public string Empresa { get; set; }
        public string Email { get; set; }
        public string TelCelular { get; set; }
        public string Password { get; set; }
        public string Direccion { get; set; }
        public string Notas { get; set; }
        public override string ToString()
        {
            return Nombre;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Entidades
{
    public class OrdenDePago:Base
    {
        public string IdProyecto { get; set; }
        public string NombreProyecto { get; set; }
        public string IdCliente { get; set; }
        public string NombreCliente { get; set; }
        public DateTime FechaDePagoEstimada { get; set; }
        public DateTime? FechaDePagoReal { get; set; }
        public string Notas { get; set; }
        public float Total { get; set;}

    }
}

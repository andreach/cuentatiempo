﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Entidades
{
    public class CostoPorHora:Base
    {
        public string Descripcion { get; set; }
        public float Costo { get; set; }
        public override string ToString()
        {
            return $"{Descripcion} ${Costo}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;

namespace CuentaTiempo.Modelos
{
    public class TrabajoListadoModel
    {
        public TrabajoListadoModel()
        {

        }
        

        public TrabajoListadoModel(TrabajoRealizado trabajo, Proyecto proyecto, CostoPorHora costo, OrdenDePago orden)
        {
            Descripcion=trabajo.Descripcion;
            IdTrabajoRealizado = trabajo.Id;
            Proyecto = proyecto.Nombre;
            Inicio = trabajo.Inicio;
            Termino = trabajo.Termino;
            if (Termino==null)
            {
                costo = null;
            }
            else
            {
                TimeSpan duracion = Termino.Value - Inicio;
                double costoPorMinuto = costo.Costo / 60;
                Monto = (float) (costoPorMinuto * duracion.TotalMinutes);

            }
            if (orden!=null)
            {
                FechaDePagoEstimada = orden.FechaDePagoEstimada;
                FechaDePagoReal = orden.FechaDePagoReal;
            }
            else
            {
                FechaDePagoEstimada = null;
                FechaDePagoReal = null;
            }
        }
        public string IdTrabajoRealizado { get; set;}
        public string Descripcion { get; set; }
        public string Proyecto { get; set; }
        public DateTime Inicio {get; set;} 
        public DateTime? Termino { get; set; }
        public float Monto { get; set; }
        public DateTime? FechaDePagoEstimada { get; set; }
        public DateTime? FechaDePagoReal { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Modelos
{
    public class DatoGrafica
    {
        public string X { get; set; }
        public float Y { get; set;  }
    }
}

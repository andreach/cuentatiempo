﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuentaTiempo.Modelos
{
    public class SerieDeGrafica
    {
        public List<DatoGrafica> Datos { get; set; }
        public string Tipo { get; set; }
        public string NombreSerie { get; set; }
    }
}

using CuentaTiempo.DAL;
using CuentaTiempo.Entidades;
using CuentaTiempo.Modelos;
using CuentaTiempo.Services;

namespace CuentaTiempo.Pages;

public partial class DashboardPage : ContentPage
{
	RepositorioGenerico<Cliente> repoClientes;
	RepositorioGenerico<Proyecto> repoProyectos;
	RepositorioGenerico<TrabajoRealizado> repoTrabajo;
	RepositorioGenerico<OrdenDePago> repoOrdenDePago;
	public DashboardPage()
	{
		InitializeComponent();
		repoClientes = FabricRepository.RepositorioCliente();
		repoProyectos = FabricRepository.RepositorioProyecto();
		repoTrabajo = FabricRepository.RepositorioTrabajoRealizado();
		repoOrdenDePago = FabricRepository.RepositorioOrdenDePago();
		

		
	}
	private string GeneraEtiqueta(DateTime fecha)
	{
		switch (fecha.Month)
		{
			case 1:
				return $"{fecha.Year} Enero";
			case 2:
				return $"{fecha.Year} Febrero";
			case 3:
                return $"{fecha.Year} Marzo";
			case 4:
                return $"{fecha.Year} Abril";
            case 5:
                return $"{fecha.Year} Mayo";
            case 6:
                return $"{fecha.Year} Junio";
            case 7:
                return $"{fecha.Year} Julio";
            case 8:
                return $"{fecha.Year} Agosto";
            case 9:
				return $"{fecha.Year} Septiembre";
            case 10:
                return $"{fecha.Year} Octubre";
            case 11:
                return $"{fecha.Year} Noviembre";
            case 12:
                return $"{fecha.Year} Diciembre";
            default:
				return fecha.Year.ToString();
		}
	}
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lblNumClientes.Text = repoClientes.Get.Count().ToString();
        lblNumProyectos.Text = repoProyectos.Get.Count().ToString();
        lblTrabajado.Text = repoTrabajo.Get.Count().ToString();
        lblNumOrdenes.Text = repoOrdenDePago.Get.Count().ToString();

        lblTrabajado.Text = $"${repoOrdenDePago.Get.Sum(o => o.Total).ToString()}";
        lblCobrado.Text = $"${repoOrdenDePago.Get.Where(t => t.FechaDePagoReal != null).Sum(o => o.Total).ToString()}";

        var ordenesCobradas = repoOrdenDePago.Query(o => o.FechaDePagoReal != null).OrderBy(f => f.FechaDePagoReal).ToList();
        List<DatosGraficoCobro> datos = new List<DatosGraficoCobro>();
        var primera = ordenesCobradas.FirstOrDefault();
        if (primera!=null)
        {
            DateTime fecha = primera.FechaDePagoReal.Value.Date;
            do
            {
                //datos.Add(new DatosGraficoCobro(fecha, ordenesCobradas.Where(o => o.FechaDePagoReal.Value.Year == fecha.Year && o.FechaDePagoReal.Value.Month == fecha.Month).Sum(t => t.Total)));
                fecha = fecha.AddMonths(1);
            } while (fecha < DateTime.Now);
        }


        if (ordenesCobradas.Count>1)
        {
            DateTime fechaInicial = ordenesCobradas[0].FechaDePagoEstimada;
            DateTime fechaFinal = DateTime.Now;
            List<DatoGrafica> datosGrafica = new List<DatoGrafica>();
            while (fechaInicial <= fechaFinal.AddMonths(1))
            {
                datosGrafica.Add(new DatoGrafica()
                {
                    X = GeneraEtiqueta(fechaInicial),
                    Y = ordenesCobradas.Where(o => o.FechaDePagoReal.Value.Year == fechaInicial.Year && o.FechaDePagoReal.Value.Month == fechaInicial.Month).Sum(t => t.Total)
                });
                fechaInicial = fechaInicial.AddMonths(1);
            }
            Chart.Html = GeneradorGraficas.GeneraGrafica(datosGrafica, "Ordenes Cobradas por mes", "scatter", 1200);
        }
       

    }
}
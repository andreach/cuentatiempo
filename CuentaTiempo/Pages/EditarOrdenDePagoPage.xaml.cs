using CuentaTiempo.DAL;
using CuentaTiempo.Entidades;
using CuentaTiempo.Modelos;
namespace CuentaTiempo.Pages;

public partial class EditarOrdenDePago : ContentPage
{
    RepositorioGenerico<Cliente> repoClientes;
    RepositorioGenerico<Proyecto> repoProyectos;
    RepositorioGenerico<TrabajoRealizado> repoTrabajos;
    RepositorioGenerico<CostoPorHora> repoCostoPorHora;
    RepositorioGenerico<OrdenDePago> repoOrdeDePago;
    RepositorioGenerico<Extras> repoExtras;
    List<Cliente> clientes;
    List<Proyecto> proyectos;
    List<TrabajoRealizado> trabajos;
    List<CostoPorHora> costos;
    List<TrabajoListadoModel> trabajosPorCobrar;
    List<Extras> extras;
    OrdenDePago orden;
    Proyecto proyectoSeleccionado;
    bool esEdicion=false;
	public EditarOrdenDePago(Entidades.OrdenDePago ordenDePago)
	{
		InitializeComponent();
        repoClientes = FabricRepository.RepositorioCliente();
        repoProyectos = FabricRepository.RepositorioProyecto();
        repoTrabajos = FabricRepository.RepositorioTrabajoRealizado();
        repoOrdeDePago = FabricRepository.RepositorioOrdenDePago();
        repoCostoPorHora = FabricRepository.RepositorioCostoPorHora();
        repoExtras = FabricRepository.RepositorioExtras();
        trabajosPorCobrar = new List<TrabajoListadoModel>();
        clientes = repoClientes.Get.ToList();
        costos = repoCostoPorHora.Get.ToList();
        pkrClientes.ItemsSource = clientes;
        extras=new List<Extras>();
        orden = ordenDePago;
        BindingContext = orden;
        esEdicion = string.IsNullOrEmpty(orden.Id);
        dpExtra.Date = DateTime.Now;
        tpExtra.Time = DateTime.Now.TimeOfDay;
        
        if (esEdicion)
        {
            Title = $"Editar orden de pago para {orden.NombreCliente}";
            pkrClientes.SelectedItem = clientes.Where(c => c.Id == orden.IdCliente).SingleOrDefault();
            proyectoSeleccionado = repoProyectos.Query(p => p.Nombre == orden.NombreProyecto).SingleOrDefault();
            pkrProyectos.SelectedItem = proyectoSeleccionado.Nombre;

            //cargar la lista de trabajos
            var trabs = repoTrabajos.Query(t => t.IdOrdenDePago == orden.Id).ToList();
            foreach (var item in trabs)
            {
                trabajosPorCobrar.Add(new TrabajoListadoModel(item, proyectoSeleccionado, costos.Where(c => c.Id == item.IdCostoPorHora).SingleOrDefault(), null));
            }
            lstTrabajos.ItemsSource = trabajosPorCobrar;
            //cargar la lista de extras
            extras = repoExtras.Query(e => e.IdOrdenDePago == orden.Id).ToList();
            lstExtras.ItemsSource = extras;
            swtPagoReal.IsToggled = orden.FechaDePagoReal != null;
            CalcularTotal();
        }
        else
        {
            Title = "Nueva Orden de Pago";
            orden.FechaDePagoReal = DateTime.Now;
            orden.FechaDePagoEstimada = DateTime.Now;
            dtpPagoEstimado.Date = DateTime.Now;
        }
        btnEliminar.IsVisible = esEdicion;
	}

    private void pkrClientes_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cliente cliente = pkrClientes.SelectedItem as Cliente;
        if (cliente !=null)
        {
            proyectos = repoProyectos.Query(p=>p.IdCliente==cliente.Id).ToList();
            pkrProyectos.ItemsSource = null;
            pkrProyectos.ItemsSource = proyectos;
            pkrProyectos.ItemsSource = pkrProyectos.GetItemsAsArray();
        }
        
    }

    private void pkrProyectos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Proyecto proyecto = proyectos.Where(p=>p.Nombre== pkrProyectos.SelectedItem).SingleOrDefault();
        if (proyecto!=null)
        {
            proyectoSeleccionado = proyecto;
            trabajos = repoTrabajos.Query(t => t.IdProyecto == proyecto.Id && string.IsNullOrEmpty(t.IdOrdenDePago)).ToList();
            pkrTrabajo.ItemsSource = null;
            pkrTrabajo.ItemsSource = trabajos;
            pkrTrabajo.ItemsSource = pkrTrabajo.GetItemsAsArray();
        }
    }

    private void btnAgregarTrabajo_Clicked(object sender, EventArgs e)
    {
        TrabajoRealizado trabajo = trabajos.Where(t => t.Descripcion == pkrTrabajo.SelectedItem).SingleOrDefault();
        if (trabajo!=null)
        {
            trabajosPorCobrar.Add(new TrabajoListadoModel(trabajo,proyectoSeleccionado,costos.Where(c=>c.Id==trabajo.IdCostoPorHora).SingleOrDefault(),null));
            lstTrabajos.ItemsSource = null;
            lstTrabajos.ItemsSource = trabajosPorCobrar;
            CalcularTotal();
        }
    }
    private void CalcularTotal()
    {
        orden.Total= (trabajosPorCobrar.Sum(t => t.Monto) + extras.Sum(s => s.Costo));
        lblTotal.Text = orden.Total.ToString();
    }

    private void btnAgregarExtra_Clicked(object sender, EventArgs e)
    {
        Extras extra = new Extras()
        {
            Costo = float.Parse(entMonto.Text),
            Descripcion = entDescripcion.Text,
            FechaHora = DateTime.Now,
            FechaHoraEvento = new DateTime(dpExtra.Date.Year, dpExtra.Date.Month, dpExtra.Date.Day, tpExtra.Time.Hours, tpExtra.Time.Minutes, tpExtra.Time.Seconds),
            IdProyecto = proyectoSeleccionado.Id,
            Notas = entNotas.Text
        };
        extras.Add(extra);
        lstExtras.ItemsSource = null;
        lstExtras.ItemsSource = extras;
        CalcularTotal();
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Proyecto proy;

        proy =(pkrProyectos.SelectedItem as Proyecto);
        if (proy==null)
        {
            proy = repoProyectos.Query(p => p.Nombre == pkrProyectos.SelectedItem.ToString()).FirstOrDefault();
        }
        orden.IdProyecto = proy.Id;
        orden.NombreProyecto = proy.Nombre;
        var cliente = pkrClientes.SelectedItem as Cliente;
        orden.IdCliente = cliente.Id;
        orden.NombreCliente = cliente.Nombre;
        orden.FechaDePagoEstimada = dtpPagoEstimado.Date;
        if (swtPagoReal.IsToggled)
        {
            orden.FechaDePagoReal = dtpPagoReal.Date;
        }
        else
        {
            orden.FechaDePagoReal = null;
        }

        orden.Notas = entNotasGenerales.Text;
        
        OrdenDePago ordenGuardada = esEdicion ? repoOrdeDePago.Update(orden.Id, orden) : repoOrdeDePago.Create(orden);
        foreach (var item in extras)
        {
            item.IdOrdenDePago = "";
            repoExtras.Delete(item.Id);

        }
        

                if (ordenGuardada!=null)
            {
                foreach (var trabajo in trabajosPorCobrar)
                {
                var t = repoTrabajos.GetById(trabajo.IdTrabajoRealizado);
                    repoTrabajos.Update(t.Id, t);

                }
                foreach(var extra in extras)
                {
                    extra.IdOrdenDePago = ordenGuardada.Id;
                    extra.IdProyecto = ordenGuardada.IdProyecto;
                    repoExtras.Create(extra);
                }
            DisplayAlert("Cuenta tiempo", "Orden guardada correctamente", "Ok");
            Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Error: ", repoOrdeDePago.Error, "Ok");
            }
        
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            if (orden.FechaDePagoReal == null)
            {

                var r = await DisplayAlert("Confirma", $"Deseas eliminar la orden de pago actual?", "Si", "No");
                if (r)
                {
                    //eliminar los extras de la orden
                    foreach (var item in extras)
                    {
                        repoExtras.Delete(item.Id);
                    }
                    //quitar el id orden a los trabajos
                    foreach (var item in repoTrabajos.Query(t => t.IdOrdenDePago == orden.Id))
                    {
                        item.IdOrdenDePago = null;
                        repoTrabajos.Update(item.Id, item);
                    }
                    //eliminar la orden
                    repoOrdeDePago.Delete(orden.Id);
                    await Navigation.PopAsync();
                }
            }
            else
            {
                await DisplayAlert("Error", "No puedes eliminar una orden ya pagada", "Ok");
            }
            
        }
        catch (Exception ex)
        {
            await DisplayAlert("Error", ex.Message, "Ok");
            
        }
        
    }
}
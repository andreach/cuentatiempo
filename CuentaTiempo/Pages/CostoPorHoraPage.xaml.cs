using CuentaTiempo.Entidades;
using CuentaTiempo.DAL;
using CuentaTiempo.Validadores;

namespace CuentaTiempo.Pages;

public partial class CostoPorHoraPage : ContentPage
{
    RepositorioGenerico<CostoPorHora> repositorio;
	public CostoPorHoraPage()
	{
		InitializeComponent();
        repositorio = FabricRepository.RepositorioCostoPorHora();
	}
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstCostos.ItemsSource = null;
        lstCostos.ItemsSource = repositorio.Get;
    }

    private void btnAgregar_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarCostoPorHoraPage(new CostoPorHora()));
    }


    private void lstCostos_ItemTapped(object sender, ItemTappedEventArgs e)
    {
        CostoPorHora costo = lstCostos.SelectedItem as CostoPorHora;
        if (costo!=null)
        {
            Navigation.PushAsync(new EditarCostoPorHoraPage(costo));
        }
    }
}
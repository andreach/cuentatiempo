using CuentaTiempo.DAL;
using CuentaTiempo.Entidades;
namespace CuentaTiempo.Pages;

public partial class EditarCostoPorHoraPage : ContentPage
{
	CostoPorHora costo;
	bool esEdicion = false;
	RepositorioGenerico<CostoPorHora> repo;
	public EditarCostoPorHoraPage(CostoPorHora costo)
	{
		InitializeComponent();
		repo = FabricRepository.RepositorioCostoPorHora();
		this.costo = costo;
		BindingContext = costo;
		if (string.IsNullOrEmpty(costo.Id))
		{
			esEdicion = false;
			Title = "Nuevo Costo por Hora";
		}
		else
		{
			esEdicion = true;
			Title = $"Editar {costo.Descripcion}";
		}
	}

    private void btnEliminar_Clicked(object sender, EventArgs e)
    {
		if (DisplayAlert("Confirme", $"�Realmente desea eliminar el costo {costo.Descripcion}", "Si", "No").Result)
		{
			if (repo.Delete(costo.Id))
			{
                DisplayAlert("Cuenta tiempo", "Registro eliminado correctamente", "Ok");
				Navigation.PopAsync();
			}
			else
			{
                DisplayAlert("Cuenta tiempo", $"Error :{repo.Error}", "Ok");
            }
		}
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
		CostoPorHora c = esEdicion ? repo.Update(costo.Id, costo) : repo.Create(costo);
		if (c!=null)
		{
			DisplayAlert("Cuenta tiempo", "Registro guardado correctamente", "Ok");
			Navigation.PopAsync();
		}
		else
		{
            DisplayAlert("Cuenta tiempo", $"Error:{repo.Error}", "Ok");
        }
    }
}
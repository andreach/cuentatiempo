using CuentaTiempo.DAL;
using CuentaTiempo.Entidades;
namespace CuentaTiempo.Pages;

public partial class OrdenesDePagoPage : ContentPage
{
    RepositorioGenerico<OrdenDePago> repo;
    List<OrdenDePago> datos;
    List<string> filtros;
	public OrdenesDePagoPage()
	{
		InitializeComponent();
        repo = FabricRepository.RepositorioOrdenDePago();
        filtros = new List<string>();
        

    }
    protected override void OnAppearing()
    {
        base.OnAppearing();
        datos = repo.Get.ToList();
        lstOrdenes.ItemsSource = null;
        lstOrdenes.ItemsSource = datos;
        filtros.Clear();
        filtros.Add("Todas");
        filtros.Add("Pagadas");
        filtros.Add("Por Pagar");
        filtros.Add("Vencidas");
        filtros.Add("Ultima semana");
        filtros.Add("Ultimo mes");
        
        var clientes = datos.Select(x => x.NombreCliente).Distinct().ToList();
        foreach (var item in clientes)
        {
            filtros.Add($"Cliente {item}");
        }
        var proyectos = datos.Select(x => x.NombreProyecto).Distinct().ToList();
        foreach (var item in proyectos)
        {
            filtros.Add($"Proyecto {item}");
        }
        pkrFiltros.ItemsSource = filtros;
        pkrFiltros.ItemsSource = pkrFiltros.GetItemsAsArray();
    }

    private void btnBuscar_Clicked(object sender, EventArgs e)
    {
        var fil = pkrFiltros.SelectedItem;
        if (fil!=null)
        {
            string filtro = pkrFiltros.SelectedItem.ToString();

            if (!string.IsNullOrEmpty(filtro))
            {
                lstOrdenes.ItemsSource = null;
                switch (filtro)
                {
                    case "Todas":
                        lstOrdenes.ItemsSource = repo.Get;
                        break;
                    case "Pagadas":
                        lstOrdenes.ItemsSource = repo.Query(o => o.FechaDePagoReal != null);
                        break;
                    case "Por Pagar":
                        lstOrdenes.ItemsSource = repo.Query(o => o.FechaDePagoReal == null);
                        break;
                    case "Vencidas":
                        lstOrdenes.ItemsSource = repo.Query(o => o.FechaDePagoReal == null && o.FechaDePagoEstimada < DateTime.Now);
                        break;
                    case "Ultima semana":
                        lstOrdenes.ItemsSource = repo.Query(o => o.FechaHora > DateTime.Now.AddDays(-7));
                        break;
                    case "Ultimo mes":
                        lstOrdenes.ItemsSource = repo.Query(o => o.FechaHora > DateTime.Now.AddMonths(-1));
                        break;
                    default:
                        if (filtro.Contains("Cliente "))
                        {
                            string cliente = filtro.Replace("Cliente ", "");
                            lstOrdenes.ItemsSource = repo.Query(o => o.NombreCliente == cliente);
                        }
                        if (filtro.Contains("Proyecto "))
                        {
                            string proyecto = filtro.Replace("Proyecto ", "");
                            lstOrdenes.ItemsSource = repo.Query(o => o.NombreProyecto == proyecto);
                        }
                        break;

                }
            }
        }
    }

    private void btnNuevo_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarOrdenDePago(new OrdenDePago()));
    }

    private void lstOrdenes_ItemTapped(object sender, ItemTappedEventArgs e)
    {
        OrdenDePago orden = e.Item as OrdenDePago;
        if (orden!=null)
        {
            Navigation.PushAsync(new EditarOrdenDePago(orden));
        }
    }
}
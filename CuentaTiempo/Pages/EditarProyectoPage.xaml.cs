using CuentaTiempo.Entidades;
using CuentaTiempo.DAL;
namespace CuentaTiempo.Pages;

public partial class EditarProyectoPage : ContentPage
{
	Proyecto proyecto;
	RepositorioGenerico<Proyecto> repositorio;
	RepositorioGenerico<Cliente> repoCliente;
	bool esEdicion=false;
	List<Cliente> clientes;
	public EditarProyectoPage(Proyecto proyecto)
	{
		InitializeComponent();
		this.proyecto = proyecto;
		repositorio = FabricRepository.RepositorioProyecto();
		repoCliente = FabricRepository.RepositorioCliente();
		clientes = repoCliente.Get.ToList();
		pkrCliente.ItemsSource = clientes;

		if (string.IsNullOrEmpty(proyecto.Id))
		{
			Title = "Agregar proyecto";
			btnEliminar.IsVisible = false;
			proyecto.FechaInicio = DateTime.Now;
			btnTerminado.IsToggled = false;
			esEdicion=false;
		}
		else
		{
			Title = $"Editar Proyecto {proyecto.Nombre}";
			btnEliminar.IsVisible = true;
			pkrCliente.SelectedItem=clientes.Where(c=>c.Id==proyecto.IdCliente).SingleOrDefault();
			if (proyecto.FechaTermino!=null)
			{
				btnTerminado.IsToggled = true;
				pkrFechaTermino.Date = proyecto.FechaTermino.Value;
			}
			esEdicion=true;
		}
		BindingContext=proyecto;
	}

    private void btnEliminar_Clicked(object sender, EventArgs e)
    {
		if(DisplayAlert("Confirmacion", $"�Realmente deseas eliminar el proyecto {proyecto.Nombre}?", "Si", "No").Result)
		{
			if (repositorio.Delete(proyecto.Id))
			{
				DisplayAlert("Cuenta tiempo", "Registro eliminado correctamente", "Ok");
			}
			else
			{
				DisplayAlert("Cuente tiempo", $"Error:{repositorio.Error}", "Ok");
			}
		}
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
		proyecto.IdCliente = (pkrCliente.SelectedItem as Cliente).Id;
		if (btnTerminado.IsToggled)
		{
			proyecto.FechaTermino = pkrFechaTermino.Date;
		}
		else
		{
			proyecto.FechaTermino=null;
		}
		Proyecto r = esEdicion ? repositorio.Update(proyecto.Id, proyecto) : repositorio.Create(proyecto);
		if (r!=null)
		{
			DisplayAlert("Cuenta tiempo", "Registro guardado correctamente", "Ok");
			Navigation.PopAsync();
		}
		else
		{
            DisplayAlert("Cuenta tiempo", $"Error:{repositorio.Error}", "Ok");
        }
    }
}
using CuentaTiempo.DAL;
using CuentaTiempo.Entidades;
using CuentaTiempo.Validadores;
namespace CuentaTiempo.Pages;

public partial class ClientesPage : ContentPage
{
	RepositorioGenerico<Cliente> repositorioClientes;
	IEnumerable<Cliente> clientes;
	public ClientesPage()
	{
		InitializeComponent();
		repositorioClientes =FabricRepository.RepositorioCliente();
		
	}
	protected override void OnAppearing()
	{
		base.OnAppearing();
		lstClientes.ItemsSource = null;
		lstClientes.ItemsSource = repositorioClientes.Get;
	}
    private void btnAgregar_Clicked(object sender, EventArgs e)
    {
		Navigation.PushAsync(new EditarClientePage(new Cliente()));
    }

    private void lstClientes_ItemTapped(object sender, ItemTappedEventArgs e)
    {
		Cliente cliente = e.Item as Cliente;
		if (cliente!=null)
		{
			Navigation.PushAsync(new EditarClientePage(cliente));
		}
    }
}
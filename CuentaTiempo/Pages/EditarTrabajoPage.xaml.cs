using CuentaTiempo.Entidades;
using CuentaTiempo.DAL;
namespace CuentaTiempo.Pages;

public partial class EditarTrabajoPage : ContentPage
{
    RepositorioGenerico<TrabajoRealizado> repoTrabajo;
    RepositorioGenerico<CostoPorHora> repoCostoPorHora;
    RepositorioGenerico<Proyecto> repoProyecto;
    RepositorioGenerico<OrdenDePago> repoOrdenDePago;
    bool esEdicion = false;
    bool esModificable = false;
    TrabajoRealizado trabajoRealizado;
    List<Proyecto> proyectos;
    List<CostoPorHora> costos;
	public EditarTrabajoPage(TrabajoRealizado trabajo)
	{
		InitializeComponent();
        trabajoRealizado = trabajo;
        BindingContext = trabajoRealizado;
        repoTrabajo = FabricRepository.RepositorioTrabajoRealizado();
        repoProyecto = FabricRepository.RepositorioProyecto();
        repoOrdenDePago = FabricRepository.RepositorioOrdenDePago();
        repoCostoPorHora = FabricRepository.RepositorioCostoPorHora();
        proyectos= repoProyecto.Get.ToList();

        pkrProyecto.ItemsSource = proyectos;
        costos= repoCostoPorHora.Get.ToList();
        pkrTipoTrabajo.ItemsSource = costos;

        if (string.IsNullOrEmpty(trabajo.Id))
        {
            esEdicion = false;
            Title = "Nuevo trabajo realizado";
            pkrFechaInicio.Date = DateTime.Now;
            pkrHoraInicio.Time = DateTime.Now.TimeOfDay;
            swtTerminado.IsToggled = false;
            pkrFechaFin.Date = DateTime.Now;
            pkrHoraFin.Time=DateTime.Now.TimeOfDay;
        }
        else
        {
            pkrProyecto.SelectedItem=proyectos.Where(p=>p.Id==trabajoRealizado.IdProyecto).SingleOrDefault();
            pkrTipoTrabajo.SelectedItem=costos.Where(c=>c.Id==trabajoRealizado.IdCostoPorHora).SingleOrDefault();
            esEdicion = true;
            Title=$"Editar trabajo {trabajo.Descripcion}";
            pkrFechaInicio.Date = trabajo.Inicio;
            pkrHoraInicio.Time = trabajo.Inicio.TimeOfDay;
            swtTerminado.IsToggled=trabajo.Termino!=null;
            if (trabajo.Termino!=null)
            {
                pkrFechaFin.Date = trabajo.Termino.Value;
                pkrHoraFin.Time = trabajo.Termino.Value.TimeOfDay;
            }
            else
            {
                pkrFechaFin.Date = DateTime.Now;
                pkrHoraFin.Time = DateTime.Now.TimeOfDay;
            }
            
        }
        if (string.IsNullOrEmpty(trabajo.IdOrdenDePago))
        {
            esModificable = true;
        }
        else
        {
            esModificable = false;
            OrdenDePago orden = repoOrdenDePago.GetById(trabajo.IdOrdenDePago);
            lblFechaEstimadaPago.Text = orden.FechaDePagoEstimada.ToShortDateString();
            if (orden.FechaDePagoReal!=null)
            {
                lblFechaRealPago.Text = orden.FechaDePagoReal.Value.ToShortDateString();
            }
            btnGuardar.IsVisible = false;
            btnEliminar.IsVisible = false;
        }
        
	}

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Proyecto p = pkrProyecto.SelectedItem as Proyecto;
        if (p!=null)
        {
            trabajoRealizado.IdProyecto = p.Id;
        }
        CostoPorHora c = pkrTipoTrabajo.SelectedItem as CostoPorHora;
        if (c!=null)
        {
            trabajoRealizado.IdCostoPorHora = c.Id;
        }

       
        DateTime fechaInicio = pkrFechaInicio.Date;
        TimeSpan horaInicio = pkrHoraInicio.Time;
        trabajoRealizado.Inicio = new DateTime(fechaInicio.Year, fechaInicio.Month, fechaInicio.Day, horaInicio.Hours, horaInicio.Minutes, horaInicio.Seconds);
        if (swtTerminado.IsToggled)
        {
            DateTime fechaFin = pkrFechaFin.Date;
            TimeSpan horaFin = pkrHoraFin.Time;
            trabajoRealizado.Termino=new DateTime(fechaFin.Year, fechaFin.Month, fechaFin.Day, horaFin.Hours, horaFin.Minutes, horaFin.Seconds);

        }
        else
        {
            trabajoRealizado.Termino = null;
        }
        TrabajoRealizado t = esEdicion ? repoTrabajo.Update(trabajoRealizado.Id, trabajoRealizado) : repoTrabajo.Create(trabajoRealizado);
        if (t!=null)
        {
            DisplayAlert("Cuenta Tiempo", "Registro Guardado Correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Cuenta Tiempo", $"Error: {repoTrabajo.Error}", "Ok");
        }
    }

    private void btnEliminar_Clicked(object sender, EventArgs e)
    {
        if (DisplayAlert("Confirmacion", $"�Realmente deseas eliminar el trabajo{trabajoRealizado.Descripcion}'", "Si", "No").Result)
        {
            if (repoTrabajo.Delete(trabajoRealizado.Id))
            {
                DisplayAlert("Cuenta Tiempo", "Registro eliminado correctamente", "Ok");
                Navigation.PopAsync();
            }
            else
            {
                DisplayAlert("Cuenta Tiempo", $"Error: {repoTrabajo.Error}", "Ok");
            }
        }
    }
}
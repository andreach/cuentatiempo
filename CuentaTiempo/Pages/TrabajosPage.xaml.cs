using CuentaTiempo.Entidades;
using CuentaTiempo.DAL;
using CuentaTiempo.Modelos;

namespace CuentaTiempo.Pages;

public partial class TrabajosPage : ContentPage
{
    RepositorioGenerico<TrabajoRealizado> repositorio;
    RepositorioGenerico<OrdenDePago> repoOrdenDePago;
    RepositorioGenerico<Proyecto> repoProyecto;
    RepositorioGenerico<CostoPorHora> repoCostoPorHora;
    List<TrabajoListadoModel> datos;
    List<OrdenDePago> ordenes;
    List<Proyecto> proyectos;
    List<TrabajoRealizado> trabajos;
    List<CostoPorHora> costos;
	public TrabajosPage()
	{
		InitializeComponent();
        repositorio = FabricRepository.RepositorioTrabajoRealizado();
        repoOrdenDePago = FabricRepository.RepositorioOrdenDePago();
        repoProyecto = FabricRepository.RepositorioProyecto();
        repoCostoPorHora = FabricRepository.RepositorioCostoPorHora();
	}
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstTrabajos.ItemsSource = null;
        ordenes = repoOrdenDePago.Get.ToList();
        proyectos = repoProyecto.Get.ToList();
        trabajos = repositorio.Get.ToList();
        costos = repoCostoPorHora.Get.ToList();
        datos = new List<TrabajoListadoModel>();
        foreach (var trabajo in trabajos)
        {
            OrdenDePago orden =ordenes.Where(o=>o.Id==trabajo.IdOrdenDePago).SingleOrDefault();
            Proyecto proyecto = proyectos.Where(p => p.Id == trabajo.IdProyecto).SingleOrDefault();
            CostoPorHora costo = costos.Where(c => c.Id == trabajo.IdCostoPorHora).SingleOrDefault();
            datos.Add(new TrabajoListadoModel(trabajo, proyecto, costo, orden));
            
        }
        lstTrabajos.ItemsSource = datos.OrderBy(t=>t.FechaDePagoReal);
    }
    private void btnAgregar_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarTrabajoPage(new TrabajoRealizado()));
    }

    private void lstTrabajos_ItemTapped(object sender, ItemTappedEventArgs e)
    {
        TrabajoListadoModel t = e.Item as TrabajoListadoModel;
        if (t!=null)
        {
            Navigation.PushAsync(new EditarTrabajoPage(trabajos.Where(trab=>trab.Id==t.IdTrabajoRealizado).SingleOrDefault()));
        }
    }
}
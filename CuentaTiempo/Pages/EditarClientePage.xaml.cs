using CuentaTiempo.Entidades;
using CuentaTiempo.DAL;
using CuentaTiempo.Validadores;

namespace CuentaTiempo.Pages;

public partial class EditarClientePage : ContentPage
{
	Cliente cliente;
	bool esEdicion = false;
	RepositorioGenerico<Cliente> repo;
	public EditarClientePage(Cliente cliente)
	{
		InitializeComponent();
		repo = FabricRepository.RepositorioCliente();
		this.cliente = cliente;
		esEdicion = !string.IsNullOrEmpty(cliente.Id);
		if (esEdicion)
		{
			this.Title = $"Editanto a {cliente.Nombre}";
			btnEliminar.IsVisible = true;
		}
		else
		{
			this.Title = $"Nuevo Cliente";
			btnEliminar.IsVisible = false;
		}
		BindingContext = cliente;
	}

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
		Cliente r;
		if (esEdicion)
		{
			r = repo.Update(cliente.Id, cliente);
		}
		else
		{
			r = repo.Create(cliente);
		}
		if (r!= null)
		{
            DisplayAlert("Cuenta tiempo", "Registro guardado correctamente", "Ok");
			Navigation.PopAsync();
        }
		else
		{
			DisplayAlert("Cuenta tiempo", $"Error:{repo.Error}", "Ok");
		}
    }

    private void btnEliminar_Clicked(object sender, EventArgs e)
    {
		if (DisplayAlert("Confirma", $"�Realmente deseas eliminar a {cliente.Nombre}?", "Si", "No").Result)
		{
			if (repo.Delete(cliente.Id))
			{
				DisplayAlert("Cuenta tiempo", "Elemento eliminado", "Ok");
			}
			else
			{
				DisplayAlert("Cuenta tiempo", $"Error: {repo.Error}", "Ok");
			}
		}
    }
}
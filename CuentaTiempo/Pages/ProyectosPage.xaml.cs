using CuentaTiempo.Entidades;
using CuentaTiempo.DAL;
namespace CuentaTiempo.Pages;

public partial class ProyectosPage : ContentPage
{
    RepositorioGenerico<Proyecto> repositorioProyecto;
    public ProyectosPage()
	{
		InitializeComponent();
        repositorioProyecto = FabricRepository.RepositorioProyecto();
	}
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstProyectos.ItemsSource = null;
        lstProyectos.ItemsSource = repositorioProyecto.Get;
    }
    private void lstProyectos_ItemTapped(object sender, ItemTappedEventArgs e)
    {
        Proyecto proyecto=e.Item as Proyecto;
        if (proyecto !=null)
        {
            Navigation.PushAsync(new EditarProyectoPage(proyecto));
        }
    }

    private void btnAgregar_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarProyectoPage(new Proyecto()));
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using FluentValidation;

namespace CuentaTiempo.Validadores
{
    public class ClienteValidator:BaseValidator<Cliente>
    {
        public ClienteValidator()
        {
            RuleFor(c => c.Empresa).NotEmpty().MaximumLength(50);
            RuleFor(c => c.TelCelular).NotEmpty();
            RuleFor(c => c.Direccion).NotEmpty().MaximumLength(300);
            RuleFor(c => c.Email).NotEmpty().EmailAddress();
            RuleFor(c => c.Nombre).NotEmpty().MaximumLength(150);
            //RuleFor(c => c.Password).NotEmpty();

        }
    }
}

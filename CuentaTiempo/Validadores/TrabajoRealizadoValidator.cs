﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using FluentValidation;

namespace CuentaTiempo.Validadores
{
    public class TrabajoRealizadoValidator:BaseValidator<TrabajoRealizado>
    {
        public TrabajoRealizadoValidator()
        {
            RuleFor(t => t.IdProyecto).NotEmpty();
            RuleFor(t => t.IdCostoPorHora).NotEmpty();
            RuleFor(t => t.Inicio).NotEmpty();
            RuleFor(t => t.Descripcion).NotEmpty();
        }
    }
}

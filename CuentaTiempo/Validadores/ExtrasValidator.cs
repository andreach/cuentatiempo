﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using FluentValidation;

namespace CuentaTiempo.Validadores
{
    public class ExtrasValidator:BaseValidator<Extras>
    {
        public ExtrasValidator()
        {
            RuleFor(e => e.IdProyecto).NotEmpty();
            RuleFor(e => e.FechaHoraEvento).NotEmpty();
            RuleFor(e => e.Descripcion).NotEmpty();
            RuleFor(e => e.Costo).NotEmpty().GreaterThan(0);

        }
    }
}

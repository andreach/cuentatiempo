﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using FluentValidation;

namespace CuentaTiempo.Validadores
{
    public class CostoPorHoraValidator:BaseValidator<CostoPorHora>
    {
        public CostoPorHoraValidator()
        {
            RuleFor(c => c.Descripcion).NotEmpty();
            RuleFor(c => c.Costo).NotEmpty().GreaterThan(0);
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CuentaTiempo.Entidades;
using FluentValidation;

namespace CuentaTiempo.Validadores
{
    public class ProyectoValidator:BaseValidator<Proyecto>
    {
        public ProyectoValidator()
        {
            RuleFor(p => p.Nombre).NotEmpty().MaximumLength(200);
            RuleFor(p => p.IdCliente).NotEmpty();
            RuleFor(p => p.Descripcion).NotEmpty();
            RuleFor(p => p.FechaInicio).NotEmpty();
        }
    }
}

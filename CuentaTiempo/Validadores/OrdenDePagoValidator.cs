﻿using CuentaTiempo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace CuentaTiempo.Validadores
{
    public class OrdenDePagoValidator:BaseValidator<OrdenDePago>
    {
        public OrdenDePagoValidator()
        {
            RuleFor(o=>o.IdCliente).NotEmpty();
            RuleFor(o=>o.NombreCliente).NotEmpty();
            RuleFor(o=>o.IdProyecto).NotEmpty();
            RuleFor(o=>o.NombreProyecto).NotEmpty();
            RuleFor(o=>o.FechaDePagoEstimada).NotEmpty();
            RuleFor(o=>o.Total).NotEmpty().GreaterThan(0);
        }
    }
}
